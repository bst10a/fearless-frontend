window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }

        loading.classList.add('d-none')
        selectTag.classList.remove('d-none')
    }

    const formTag = document.getElementById('create-attendee-form') // Id changed
    formTag.addEventListener('submit', async event => {
        event.preventDefault()

        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))

        const locationUrl = await 'http://localhost:8001/api/attendees/' //this changed
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        }

        let attendeeForm = document.getElementById('create-attendee-form')
        attendeeForm.classList.add('d-none')
        const successTag = document.getElementById('success-message')
        successTag.classList.remove('d-none')

        let selector = document.querySelector('#conference')
        selector.classList.remove('d-none')

        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok) {
            formTag.reset()
            const newConference = await response.json()
            console.log(newConference)
        }
    })

});
